using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    [SerializeField] private Transform BackgroundTransform;

    private const float BACKGROUND_SCROLLING_SPEED = 0.5f;
    
    private void Update()
    {
        BackgroundTransform.position -= new Vector3(0f, Time.deltaTime * BACKGROUND_SCROLLING_SPEED, 0f);
    }
    
}
