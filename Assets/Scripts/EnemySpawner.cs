using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Enemy AsteroidPrefab;
    [SerializeField] private Enemy SinShipPrefab;
    [SerializeField] private Enemy ShooterShipPrefab;

    private const float MIN_TIME_BETWEEN_SPAWNS = 0.4f;
    private const float MAX_TIME_BETWEEN_SPAWNS = 1f;
    public const int MAX_SPAWNED_ENEMIES = 25;
    public const int MAX_SPAWNED_ENEMY_ENTITIES = 30;

    public Transform EnemiesParent { get; private set; }
    public int NumberOfSpawnedEnemies => EnemiesParent.childCount;

    private void Start() 
    {
        EnemiesParent = GameObject.FindWithTag("EnemiesParent").transform;
        // StartCoroutine(SpawnRandomly()); 
    }
    
    private IEnumerator SpawnRandomly() 
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(2.5f, 5f));
        
            var randomPick = Random.Range(0f, 10f);
            if (randomPick < 4) { SpawnEnemies(EnemyType.Asteroid, Random.Range(0, 3), new Vector2(0f, 1f));}
            else if (randomPick < 7.5f) { SpawnEnemies(EnemyType.SinShip, Random.Range(0, 3), Random.insideUnitCircle.normalized);}
            else { SpawnEnemies(EnemyType.ShooterShip, Random.Range(0, 3), Random.insideUnitCircle.normalized);}
        }
    }

    public void SpawnEnemies(EnemyType enemyType, int quantity, Vector2 direction)
    {
        var spawnPoint = GetSpawnPoint(direction);
        
        switch (enemyType)
        {
            case EnemyType.Asteroid:
                
                var numberOfAsteroids = quantity == 1 ? 3 : (quantity == 2 ? 5 : 8);
                for (var i = 0; i < numberOfAsteroids; i++)
                {
                    if (NumberOfSpawnedEnemies >= MAX_SPAWNED_ENEMIES) { Debug.LogWarning("Too many enemies."); break; }

                    var spawnPos = spawnPoint + ((Random.insideUnitCircle * 4) * new Vector2(1.4f, 0.3f));
                    var spawnedAsteroid = Instantiate(AsteroidPrefab, new Vector3(spawnPos.x, spawnPos.y, 0f), Quaternion.identity); 
                    spawnedAsteroid.transform.SetParent(EnemiesParent);
                    spawnedAsteroid.Initialize(this, direction * -1f);
                }
                
                break;
            case EnemyType.SinShip:
                
                var numberOfSinShips = quantity == 1 ? 2 : (quantity == 2 ? 3 : 5);
                for (var i = 0; i < numberOfSinShips; i++)
                {
                    if (NumberOfSpawnedEnemies >= MAX_SPAWNED_ENEMIES) { Debug.LogWarning("Too many enemies."); break; }

                    StartCoroutine(ExecuteWithDelay(0.5f * i, () => 
                    {
                        var spawnPos = spawnPoint;
                        var spawnedSinShip = Instantiate(SinShipPrefab, new Vector3(spawnPos.x, spawnPos.y, 0f), Quaternion.identity); 
                        spawnedSinShip.transform.SetParent(EnemiesParent);
                        spawnedSinShip.Initialize(this, direction * -1f);
                    }));
                }

                break;
            case EnemyType.ShooterShip:
                
                var numberOfShooterShips = quantity == 1 ? 1 : (quantity == 2 ? 2 : 3);
                for (var i = 0; i < numberOfShooterShips; i++)
                {
                    if (NumberOfSpawnedEnemies >= MAX_SPAWNED_ENEMIES) { Debug.LogWarning("Too many enemies."); break; }

                    StartCoroutine(ExecuteWithDelay(0.3f * i, () => 
                    {
                        var spawnPos = spawnPoint;
                        var spawnedShooterShip = Instantiate(ShooterShipPrefab, new Vector3(spawnPos.x, spawnPos.y, 0f), Quaternion.identity); 
                        spawnedShooterShip.transform.SetParent(EnemiesParent);
                        spawnedShooterShip.Initialize(this, direction * -1f);
                    }));
                }

                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(enemyType), enemyType, null);
        }
    }

    private IEnumerator ExecuteWithDelay(float delay, Action toExecute) 
    {
        yield return new WaitForSeconds(delay);
        toExecute?.Invoke();
    }

    private Vector2 GetSpawnPoint(Vector2 spawnDirection) 
    {
        //The spawn point is always on the edge of the spawn area (a rectangle that is slightly bigger than the play area, so the enemies spawn out of the screens).
        //To determine the spawn point, we take a point at the center of the spawn area, and create a line in the invert direction to the spawn direction that the player selected.
        //This should intersect with on of the edge of the spawn area rectangle, which will get us our spawn point.

        var spawnBounds = StaticReferences.SpawnAreaBounds;
        var spawnAreaCenter = (spawnBounds[0] + spawnBounds[1] + spawnBounds[2] + spawnBounds[3]) / 4;
        var invertSpawnDirection = (spawnAreaCenter + (spawnDirection * 1000f));
        
        for (var i = 0; i < 4; i++)
        {
            var intersection = Intersects(spawnAreaCenter, invertSpawnDirection, spawnBounds[i], spawnBounds[i == 3 ? 0 : i + 1]);
            if (intersection.intersect) { return intersection.intersectPoint; }
        }
        
        throw new Exception("Spawn point could not be found!");
    }

    //https://stackoverflow.com/questions/3746274/line-intersection-with-aabb-rectangle
    static (bool intersect, Vector2 intersectPoint) Intersects(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2)
    {
        var b = a2 - a1;
        var d = b2 - b1;
        var bDotDPerp = b.x * d.y - b.y * d.x;

        // if b dot d == 0, it means the lines are parallel so have infinite intersection points
        if (bDotDPerp == 0)
            return (false, Vector2.zero);

        var c = b1 - a1;
        var t = (c.x * d.y - c.y * d.x) / bDotDPerp;
        if (t < 0 || t > 1)
            return (false, Vector2.zero);

        var u = (c.x * b.y - c.y * b.x) / bDotDPerp;
        if (u < 0 || u > 1)
            return (false, Vector2.zero);

        return (true, a1 + t * b);
    }
}

public enum EnemyType 
{
    Asteroid,
    SinShip,
    ShooterShip
}
