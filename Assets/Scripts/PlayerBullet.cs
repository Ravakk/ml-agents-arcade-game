using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour, ITrackedEntity
{
    private static Vector3 MoveDirection => new Vector3(0f, 1f, 0f);
    private static float Speed => 10f;

    private void OnCollisionEnter2D(Collision2D other) 
    {
        GameObject.FindWithTag("Player").GetComponent<PlayerShip>().OnEnemyDestroyed();

        other.transform.SetParent(null);
        transform.SetParent(null);

        if (other.gameObject.TryGetComponent<Enemy>(out var enemy)) 
        {
            enemy.DestroySelf();
        }

        Destroy(gameObject);
    }

    private void Update() 
    {
        transform.position += MoveDirection * Speed * Time.deltaTime;
        if (StaticReferences.IsOutOfBounds(transform.position)) { Destroy(gameObject); }
    }
    
    public static void InstantiateBullet(Vector2 position, Transform parent)
    {
        var bulletObject = Instantiate(StaticReferences.PlayerBulletPrefab, position, Quaternion.identity);
        bulletObject.transform.SetParent(parent);
    }
    
    public int EntityType => 0;
    public Vector3 Position => transform.position;
    public Vector3 Velocity => MoveDirection * Speed * Time.deltaTime;
    public GameObject RootGO => gameObject;

}
