using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticReferences : MonoBehaviour
{
    [SerializeField] private PlayerBullet _PlayerBulletPrefab;
    [SerializeField] private EnemyBullet _EnemyBulletPrefab;
    
    [SerializeField] private Vector2[] _PlayAreaBounds; //TL, TR, BR, BL
    [SerializeField] private Vector2[] _SpawnAreaBounds; //TL, TR, BR, BL

    public static PlayerBullet PlayerBulletPrefab;
    public static EnemyBullet EnemyBulletPrefab;

    public static Vector2[] PlayAreaBounds { get; private set; }
    public static Vector2[] SpawnAreaBounds { get; private set; }
    
    private void Awake() 
    {
        PlayerBulletPrefab = _PlayerBulletPrefab;
        EnemyBulletPrefab = _EnemyBulletPrefab;

        PlayAreaBounds = _PlayAreaBounds;
        SpawnAreaBounds = _SpawnAreaBounds;
    }
    
    public static bool IsOutOfBounds(Vector2 pos) => pos.x < SpawnAreaBounds[0].x - 1 || pos.x > SpawnAreaBounds[1].x + 1 || pos.y < SpawnAreaBounds[2].y - 1 || pos.y > SpawnAreaBounds[1].y + 1;
    public static bool IsOutOfScreen(Vector2 pos) => pos.x < PlayAreaBounds[0].x || pos.x > PlayAreaBounds[1].x || pos.y < PlayAreaBounds[2].y || pos.y > PlayAreaBounds[1].y;
}
