using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Policies;
using Unity.MLAgents.Sensors;
using UnityEngine;

public class PlayerShip : Agent
{
    private const float MOVE_SPEED = 10f;
    private const float BULLET_HIT_REWARD = 0.5f;
    private const float COOLDOWN_AFTER_SHOOTING = 0.3f;
    private const float BULLET_SPAWN_Y_OFFSET = 0.1f;
    private const bool MANUAL_CONTROL = false;

    [SerializeField] private BehaviorParameters BehaviorParameters;

    private float ShootingCooldown { get; set; }
    private Transform FriendlyBulletsParent { get; set; }
    public Transform EnemiesParent { get; private set; }

    public int KillCounter { get; private set; }
    public int WoundedCounter { get; private set; }

    private void Start()
    {
        Screen.SetResolution(1024, 768, FullScreenMode.Windowed, 30);
        FriendlyBulletsParent = GameObject.FindWithTag("FriendlyBullets").transform;
        EnemiesParent = GameObject.FindWithTag("EnemiesParent").transform;
        OnEpisodeBegin();
    }
    
    private void Update() { ShootingCooldown = Mathf.Clamp(ShootingCooldown - Time.deltaTime, -0.01f, 10f); }

    //Used for inferences learning
    public override void Heuristic(in ActionBuffers actionsOut)
    {
        var continuousActions = actionsOut.ContinuousActions;
        var discreteActions = actionsOut.DiscreteActions;
        
        if (MANUAL_CONTROL) 
        {
            if (Input.GetKey(KeyCode.LeftArrow)) { continuousActions[0] = -1f; }
            if (Input.GetKey(KeyCode.RightArrow)) { continuousActions[0] = 1f; }
            if (Input.GetKey(KeyCode.UpArrow)) { continuousActions[1] = 1f; }
            if (Input.GetKey(KeyCode.DownArrow)) { continuousActions[1] = -1f; }
            discreteActions[0] = Input.GetKey(KeyCode.Space) ? 1 : 0;
        }

        else
        {
            var enemies = GetAllTrackedEntityChildren(EnemiesParent);
            var enemiesFuturePosition = enemies.Select(e => (e.Position + e.Velocity * 40f));
            
            var closestTarget = enemiesFuturePosition.OrderBy(e => Vector2.Distance(transform.position, new Vector2(e.x, transform.position.y))).FirstOrDefault();
            var closestThreats = enemies.Where(e => Vector2.Distance(transform.position, e.Position) < 1.5f).ToArray();

            if (closestThreats.Length > 0)
            {
                //Dodge
                var enemyDirection = Vector3.zero;
                foreach (var threat in closestThreats) { enemyDirection += threat.Position - transform.position; }

                continuousActions[0] = -enemyDirection.normalized.x;
                continuousActions[1] = -enemyDirection.normalized.y;
            }

            else
            {
                //Reposition or shoot
                var moveVector = (new Vector2(closestTarget == default ? -1.75f : closestTarget.x, -3f) - (Vector2) transform.position).normalized + new Vector2(0.2f, 0f);
                continuousActions[0] = moveVector.x;
                continuousActions[1] = moveVector.y;
            }
            
            discreteActions[0] = 1;
        }
    }
    
    public override void CollectObservations(VectorSensor sensor) 
    {
        //Player observations (2 floats)
        sensor.AddObservation(transform.position.x);
        sensor.AddObservation(transform.position.z);

        sensor.AddObservation(0f);
        // Player bullets observations (20 floats)
         var activeBullets = GetAllTrackedEntityChildren(FriendlyBulletsParent);
         for (var i = 0; i < 4; i++)
         {
             if (i < activeBullets.Length) 
             {
                 sensor.AddObservation(activeBullets[i].Position.x);
                 sensor.AddObservation(activeBullets[i].Position.y);
                 sensor.AddObservation(activeBullets[i].Velocity.x);
                 sensor.AddObservation(activeBullets[i].Velocity.y);
                 sensor.AddObservation(activeBullets[i].EntityType);
             }
        
             else
             {
                 sensor.AddObservation(0f);
                 sensor.AddObservation(0f);
                 sensor.AddObservation(0f);
                 sensor.AddObservation(0f);
                 sensor.AddObservation(0f);
             }
         }
        
        //Enemies observations (150 floats)
        var activeEnemies = GetAllTrackedEntityChildren(EnemiesParent);
        for (var i = 0; i < EnemySpawner.MAX_SPAWNED_ENEMY_ENTITIES; i++)
        {
            if (i < activeEnemies.Length) 
            {
                sensor.AddObservation(activeEnemies[i].Position.x);
                sensor.AddObservation(activeEnemies[i].Position.y);
                sensor.AddObservation(activeEnemies[i].Velocity.x);
                sensor.AddObservation(activeEnemies[i].Velocity.y);
                sensor.AddObservation(activeEnemies[i].EntityType);
            }

            else
            {
                sensor.AddObservation(0f);
                sensor.AddObservation(0f);
                sensor.AddObservation(0f);
                sensor.AddObservation(0f);
                sensor.AddObservation(0f);
            }
        }
    }
    
    public override void OnActionReceived(ActionBuffers actionBuffer)
    {
        var continuousActions = actionBuffer.ContinuousActions;
        var discreteActions = actionBuffer.DiscreteActions;
        
        var xMovement = continuousActions[0];
        var yMovement = continuousActions[1];
        var shoot = discreteActions[0] >= 0;
    
        transform.position += new Vector3(xMovement, yMovement, 0f) * (MOVE_SPEED * Time.deltaTime);
        if (shoot) { ShootBullet(); }
        
        //Remove reward when the ship is stuck on the edge of the screen.
        if (transform.position.x < StaticReferences.PlayAreaBounds[0].x) { AddReward(-0.01f); transform.position = new Vector3(StaticReferences.PlayAreaBounds[0].x, transform.position.y, transform.position.z); }
        if (transform.position.x > StaticReferences.PlayAreaBounds[1].x) { AddReward(-0.01f); transform.position = new Vector3(StaticReferences.PlayAreaBounds[1].x, transform.position.y, transform.position.z); }
        if (transform.position.y < StaticReferences.PlayAreaBounds[2].y) { AddReward(-0.01f); transform.position = new Vector3(transform.position.x, StaticReferences.PlayAreaBounds[2].y, transform.position.z); }
        if (transform.position.y > StaticReferences.PlayAreaBounds[1].y) { AddReward(-0.01f); transform.position = new Vector3(transform.position.x, StaticReferences.PlayAreaBounds[1].y, transform.position.z); }
        
        //Remove reward over time to force the ship to be more proactive
        AddReward(-0.001f);
    }
    
    //When colliding with enemy
    private void OnCollisionEnter2D(Collision2D other)
    {
        WoundedCounter++;
        AddReward(-0.3f);

        if (BehaviorParameters.Model == null) 
        {
            if (WoundedCounter >= 5) 
            {
                AddReward(-1);
                EndEpisode();
            }
        }
    }

    public override void OnEpisodeBegin() 
    {
        base.OnEpisodeBegin(); 
        WoundedCounter = 0;
        KillCounter = 0;
        foreach (var bullet in GetAllTrackedEntityChildren(FriendlyBulletsParent)) { bullet.RootGO.transform.SetParent(null); Destroy(bullet.RootGO); }
        foreach (var enemy in GetAllTrackedEntityChildren(EnemiesParent)) { enemy.RootGO.transform.SetParent(null); Destroy(enemy.RootGO); }
        transform.position = new Vector3(-1.7f, -3.4f, 0f);
    }
    
    public void OnEnemyDestroyed() 
    {
        AddReward(BULLET_HIT_REWARD);
        KillCounter++;

        if (BehaviorParameters.Model == null) 
        {
            if (KillCounter >= 80) 
            {
                AddReward(1);
                EndEpisode();
            }
        }
    }

    private void ShootBullet() 
    {
        if (ShootingCooldown > 0f) { return; }
        
        PlayerBullet.InstantiateBullet(transform.position + new Vector3(0f, BULLET_SPAWN_Y_OFFSET), FriendlyBulletsParent);
        ShootingCooldown += COOLDOWN_AFTER_SHOOTING;
    }

    private ITrackedEntity[] GetAllTrackedEntityChildren(Transform parent)
    {
        var children = new ITrackedEntity[parent.childCount];
        for (var i = 0; i < parent.childCount; i++)
        {
            if (parent.GetChild(i).TryGetComponent<ITrackedEntity>(out var trackedEntity)) 
            {
                children[i] = trackedEntity;
            }
        }

        return children;
    }
}

public interface ITrackedEntity
{
    int EntityType { get; }
    Vector3 Position { get; }
    Vector3 Velocity { get; }
    GameObject RootGO { get; }
}