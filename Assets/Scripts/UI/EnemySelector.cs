using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySelector : MonoBehaviour
{
    [SerializeField] private EnemySpawner EnemySpawner;
    [SerializeField] private EnemyType EnemyType;
    [SerializeField] private QuantityBar QuantitySelector;

    [SerializeField] private RectTransform PlayAreaCenter;
    [SerializeField] private Image DirectionalArrow;
    
    private void Awake() 
    {
        QuantitySelector.QuantitySelected += (quantity) => 
        {
            ValidateSpawnDirection(EnemyType, quantity); 
        }; 
    }

    private void ValidateSpawnDirection(EnemyType enemyType, int enemyQuantity) 
    {
        StartCoroutine(GetDirection(enemyType, (spawnVector) => 
        {
            EnemySpawner.SpawnEnemies(enemyType, enemyQuantity, spawnVector.normalized);
        })); 
    }

    private IEnumerator GetDirection(EnemyType enemyType, Action<Vector2> callBack) 
    {
        if (enemyType == EnemyType.Asteroid) { callBack?.Invoke(new Vector2(0f, 1f)); yield break; }

        DirectionalArrow.enabled = true;
        
        while (true)
        {
            var angleRad = Mathf.Atan2(Input.mousePosition.y - PlayAreaCenter.position.y, Input.mousePosition.x - PlayAreaCenter.position.x);
            var angleDeg = (180 / Mathf.PI) * angleRad;
            PlayAreaCenter.rotation = Quaternion.Euler(0, 0, angleDeg - 90f);

            if (Input.GetMouseButtonDown(1)) 
            {
                DirectionalArrow.enabled = false;
                yield break; 
            }
            
            if (Input.GetMouseButtonDown(0)) 
            {
                callBack(DirectionalArrow.rectTransform.position - PlayAreaCenter.position);
                DirectionalArrow.enabled = false;
                yield break;
            }

            yield return null;
        }
    }
}

