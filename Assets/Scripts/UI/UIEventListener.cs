using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIEventListener : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Action PointerEntered;
    public Action PointerExited;
    
    public void OnPointerEnter(PointerEventData eventData) { PointerEntered?.Invoke(); }
    public void OnPointerExit(PointerEventData eventData) { PointerExited?.Invoke(); }
}
