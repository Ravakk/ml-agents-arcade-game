using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private PlayerShip PlayerShipScript;
    [SerializeField] private TextMeshProUGUI ScoreText;
    
    private void Update() { ScoreText.text = $"Score : {(PlayerShipScript.KillCounter - (PlayerShipScript.WoundedCounter * 3))}"; }
}
