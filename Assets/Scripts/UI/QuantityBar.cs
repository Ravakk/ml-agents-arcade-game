using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuantityBar : MonoBehaviour
{
    [SerializeField] private UIEventListener LeftBarEventListener;
    [SerializeField] private UIEventListener MiddleBarEventListener;
    [SerializeField] private UIEventListener RightBarEventListener;

    [SerializeField] private Image[] BarSelectionHighlights;
    [SerializeField] private Button[] BarSelectionHighlightButtons;
    
    private const float BAR_HIGHLIGHTED_ALPHA = 1f;
    private const float BAR_UNHIGHLIGHTED_ALPHA = 0.3f;

    private int BarSelectionState { get; set; } = 0;
    public Action<int> QuantitySelected;
    
    public void Awake() 
    {
        LeftBarEventListener.PointerEntered += () => { UpdateBarSelectionState(1); }; 
        LeftBarEventListener.PointerExited += () => { UpdateBarSelectionState(0); }; 
        MiddleBarEventListener.PointerEntered += () => { UpdateBarSelectionState(2); }; 
        MiddleBarEventListener.PointerExited += () => { UpdateBarSelectionState(0); }; 
        RightBarEventListener.PointerEntered += () => { UpdateBarSelectionState(3); }; 
        RightBarEventListener.PointerExited += () => { UpdateBarSelectionState(0); };
        
        BarSelectionHighlightButtons[0].onClick.AddListener(OnQuantitySelected);
        BarSelectionHighlightButtons[1].onClick.AddListener(OnQuantitySelected);
        BarSelectionHighlightButtons[2].onClick.AddListener(OnQuantitySelected);
    }

    private void OnQuantitySelected() => QuantitySelected?.Invoke(BarSelectionState);
    
    private void UpdateBarSelectionState(int barState) 
    {
        BarSelectionState = barState;

        for (var i = 1; i < 4; i++)
        {
            var highlightColor = BarSelectionHighlights[i - 1].color;
            BarSelectionHighlights[i - 1].color = new Color(highlightColor.r, highlightColor.g, highlightColor.b, BarSelectionState >= i ? BAR_HIGHLIGHTED_ALPHA : BAR_UNHIGHLIGHTED_ALPHA);
        }
    }
}
