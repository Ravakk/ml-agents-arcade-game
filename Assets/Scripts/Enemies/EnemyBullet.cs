using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour, ITrackedEntity
{
    private static Vector3 MoveSpeed => new Vector3(0f, -1f, 0f);
    private static float Speed => 6f;
    
    public void Initialize() 
    {
        
    }
    
    private void OnCollisionEnter2D(Collision2D other) 
    {
        Destroy(gameObject);
    }

    private void Update() 
    {
        transform.position += MoveSpeed * Speed * Time.deltaTime;
        if (StaticReferences.IsOutOfBounds(transform.position)) { Destroy(gameObject);}
    }
    
    public static void InstantiateBullet(EnemySpawner enemySpawner, Vector2 position)
    {
        if (enemySpawner.NumberOfSpawnedEnemies >= EnemySpawner.MAX_SPAWNED_ENEMY_ENTITIES) { return; }

        var bulletObject = Instantiate(StaticReferences.EnemyBulletPrefab, position, Quaternion.identity);
        var enemiesParent = GameObject.FindWithTag("EnemiesParent").transform;
        bulletObject.transform.SetParent(enemiesParent);
    }

    public int EntityType => 1;
    public Vector3 Position => transform.position;
    public Vector3 Velocity => MoveSpeed * Speed * Time.deltaTime;
    public GameObject RootGO => gameObject;
}
