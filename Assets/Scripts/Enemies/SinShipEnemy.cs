using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinShipEnemy : Enemy, ITrackedEntity
{
    private static float MoveSpeed { get; set; } = 3f;
    
    public override void Initialize(EnemySpawner enemySpawner, Vector2 moveVector) 
    {
        base.Initialize(enemySpawner, moveVector);
        StartCoroutine(AnimateSin());
    }

    private void Update()
    {
        transform.position += MoveVector * MoveSpeed * Time.deltaTime;
    }

    private IEnumerator AnimateSin() 
    {
        var perVector = new Vector3(Vector2.Perpendicular(MoveVector).normalized.x, Vector2.Perpendicular(MoveVector).normalized.y, 0f);
        
        while (true)
        {
            for (float i = 0; i < 1; i += Time.deltaTime)
            {
                transform.position += perVector * Mathf.SmoothStep(-3f, 3f, i) * Time.deltaTime;
                
                yield return null;
            }
            
            for (float i = 1; i > 0; i -= Time.deltaTime)
            {
                transform.position += perVector * Mathf.SmoothStep(3f, -3f, i) * Time.deltaTime;

                yield return null;
            }

        }
    }

    public int EntityType => 3;
    public Vector3 Position => transform.position;
    public Vector3 Velocity => MoveVector * MoveSpeed * Time.deltaTime;
    public GameObject RootGO => gameObject;
}