using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidEnemy : Enemy, ITrackedEntity
{
    [SerializeField] private Sprite[] AsteroidSpriteVariations;

    private static float MoveSpeed { get; set; } = 1f;

    public override void Initialize(EnemySpawner enemySpawner, Vector2 moveVector) 
    {
        base.Initialize(enemySpawner, moveVector);
        GetComponent<SpriteRenderer>().sprite = AsteroidSpriteVariations[Random.Range(0, AsteroidSpriteVariations.Length)];
        MoveSpeed = Random.Range(0.5f, 0.8f);
    }

    private void Update() { transform.position += MoveVector * MoveSpeed * Time.deltaTime; }

    public int EntityType => 2;
    public Vector3 Position => transform.position;
    public Vector3 Velocity => MoveVector * MoveSpeed * Time.deltaTime;
    public GameObject RootGO => gameObject;
}