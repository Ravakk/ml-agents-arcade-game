using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class ShooterShipEnemy : Enemy, ITrackedEntity
{
    private const float BULLET_SPAWN_Y_OFFSET = -0.1f;
    
    private static float StandardMoveSpeed = 3f;
    private float CurrentMoveSpeed { get; set; } = StandardMoveSpeed;

    public override void Initialize(EnemySpawner enemySpawner, Vector2 moveVector) 
    {
        base.Initialize(enemySpawner, moveVector);
        StartCoroutine(Behave());
    }

    private void Update() { transform.position += MoveVector * CurrentMoveSpeed * Time.deltaTime; }

    private void Shoot() 
    {
        EnemyBullet.InstantiateBullet(EnemySpawner, transform.position + new Vector3(0f, BULLET_SPAWN_Y_OFFSET));
    }

    private IEnumerator Behave() 
    {
        while (true)
        {
            while (Math.Abs(Time.realtimeSinceStartup % 2) > 0.01f)
            {
                yield return null;
            }

            CurrentMoveSpeed = 0f;
            yield return new WaitForSeconds(0.3f);
            Shoot();
            yield return new WaitForSeconds(0.3f);
            CurrentMoveSpeed = StandardMoveSpeed;
        }
    }
    
    public int EntityType => 4;
    public Vector3 Position => transform.position;
    public Vector3 Velocity => MoveVector * CurrentMoveSpeed * Time.deltaTime;
    public GameObject RootGO => gameObject;
}