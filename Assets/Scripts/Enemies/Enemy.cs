using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public Vector3 MoveVector { get; private set; }
    protected EnemySpawner EnemySpawner { get; set; }

    public virtual void Initialize(EnemySpawner enemySpawner, Vector2 moveVector) 
    {
        MoveVector = new Vector3(moveVector.x, moveVector.y, 0f);
        EnemySpawner = enemySpawner;
    }
    
    private void FixedUpdate()
    {
        if (StaticReferences.IsOutOfBounds(transform.position)) { DestroySelf(); }
    }

    public void DestroySelf() 
    {
        Destroy(gameObject);
    }
}